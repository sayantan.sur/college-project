%% Read Image
imagen=imread('2.jpg');
imagen=imresize(imagen,[3000 3000]);
%% Show image
figure(1)
imshow(imagen);
title('INPUT IMAGE WITH NOISE')
%% Convert to gray scale
if size(imagen,3)==3 % RGB image
    imagen=rgb2gray(imagen);
end
%% Convert to binary image
threshold = graythresh(imagen)+0.2;
imagen =~im2bw(imagen,threshold);
%% Remove all object containing fewer than 30 pixels
imagen = bwareaopen(imagen,300);
pause(1)
%% Show image binary image
figure(2)
imshow(~imagen);
title('Input image after bounding box')
%% Label connected components
[L Ne]=bwlabel(imagen);
%% Measure properties of image regions
propied=regionprops(L,'BoundingBox');
a=zeros(size(propied),4);
for l=1:size(propied)
  a(l,1:4)=floor(propied(l).BoundingBox);
end
  for l=1:26
    a(((l-1)*26)+1:l*26,:)=sortrows(a(((l-1)*26)+1:l*26,:),2);
  end
  
size(propied)
hold on
%% Plot Bounding Box
for n=1:size(propied,1)
rectangle('Position',propied(n).BoundingBox,'EdgeColor','r','LineWidth',1)
end
hold off
pause (1)